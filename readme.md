# Jigsaw + Tailwind Css - Starter

## Jigsaw

http://jigsaw.tighten.co/docs/installation/

composer require tightenco/jigsaw

./vendor/bin/jigsaw init
Site initialized successfully!

./vendor/bin/jigsaw build
Site built successfully!

## Tailwind Css

https://tailwindcss.com/docs/installation

yarn add tailwindcss --dev

./node_modules/.bin/tailwind init [filename]
