@extends('_layouts.master')

@section('body')
<section class="bg-grey-lightest py-8">
    <div class="container mx-auto text-center">
        <h1 class="font-serif mb-2">Jigsaw + Tailwind CSS</h1>
        <p class="text-grey-dark uppercase tracking-wide">Starter Boilerplate</p>
    </div>
</section>
@endsection
